numero = int(input("Informe um número: "))
par_impar = ""

if numero % 2 == 0:
	par_impar = "par"
else:
	par_impar = "ímpar"

print("O número é", par_impar)