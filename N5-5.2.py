def isPrime(numero):
	
	if (numero <= 1):
		return False
	
	for i in range(2, numero):
		if(numero % i == 0):
			return False
	
	return True

num = int(input("Digite um número: "))
 
if isPrime(num):
	print(num, "é um número primo")
else:
	print(num, "não é um número primo")