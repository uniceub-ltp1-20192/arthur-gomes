import re

class Validador:

    @staticmethod
    def verificarInteiro():
        teste = False
        while teste == False:
            valor = input("Informe um inteiro:")
            v  = re.match("\d+",valor)
            if v != None:
                teste = True
        return int(valor)

    @staticmethod
    def validarOpcaoMenu(expReg):
        teste = False
        while teste == False:
            opcao = input("Informe uma opcao:")
            v  = re.match(expReg,opcao)
            if v != None:
                return opcao
            else:
                print("Opcao invalida! Infome um valor entre {} "
                .format(expReg))

    @staticmethod
    def validarValorInformado(valorAtual, textoMsg):
    	novoValor = input(textoMsg)
    	if novoValor != None and novoValor != "":
    		return novoValor
    	return valorAtual