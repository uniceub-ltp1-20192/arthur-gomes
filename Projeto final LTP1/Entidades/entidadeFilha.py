from Entidades.entidadeMae import Carro

class Ethios(Carro):
  def __init__(self, temAerofolio = "não"):
    super().__init__()
    self._temAerofolio = temAerofolio

  @property
  def temAerofolio(self):
    return self._temAerofolio

  @temAerofolio.setter
  def temAerofolio(self,temAerofolio):
    self._temAerofolio = temAerofolio

  def Velocimetro(self):
    print("Velocidade: 0 km/h")

  def __str__(self):

    return '''
    ----- Ethios -----
    Identificador: {}
    Tamanho: {}
    Modelo: {}
    Cor: {}
    Peso: {}
    Tem Aerofólio: {}
    '''.format(self.identificador,self.tamanho,
      self.modelo,self.cor,self.peso,self.temAerofolio)