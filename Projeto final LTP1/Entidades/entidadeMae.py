class Carro:
  
  def __init__(self,identificador = 0,tamanho=0,modelo="",cor="",peso=0):
    self._identificador = identificador
    self._tamanho = tamanho
    self._modelo = modelo
    self._cor = cor
    self._peso = peso

  @property
  def identificador(self):
    return self._identificador

  @identificador.setter
  def identificador(self,identificador):
    self._identificador = identificador

  @property
  def tamanho(self):
    return self._tamanho

  @tamanho.setter
  def tamanho(self,tamanho):
    self._tamanho = tamanho

  @property
  def modelo(self):
    return self._modelo

  @modelo.setter
  def modelo(self,modelo):
    self._modelo = modelo

  @property
  def cor(self):
    return self._cor

  @cor.setter
  def cor(self,cor):
    self._cor = cor

  @property
  def peso(self):
    return self._peso

  @peso.setter
  def peso(self,peso):
    self._peso = peso

  def buzinar(self):
    print("Bip bip!")

  def ignição(self):
    print("Motor ligado.")

