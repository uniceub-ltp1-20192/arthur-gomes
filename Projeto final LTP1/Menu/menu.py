from Validador.validador import Validador
from Dados.dados import Dados
from Entidades.entidadeFilha import Ethios

class Menu:
 
    @staticmethod
    def menuPrincipal():
        print("""
0 - Sair
1 - Consultar
2 - Inserir
3 - Alterar
4 - Deletar
""")
        return Validador.validarOpcaoMenu("[0-4]")
    @staticmethod
    def menuConsultar():
        print("""
0 - Voltar
1 - Consultar por Identificador
2 - Consultar por Propriedade
""")
        return Validador.validarOpcaoMenu("[0-2]")

    @staticmethod
    def iniciarMenu():
        d = Dados()
        opMenu = ""
        while opMenu != "0":

            opMenu = Menu.menuPrincipal()

            if opMenu == "1":

                while opMenu != "0":
                    opMenu =  Menu.menuConsultar()
                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        if(retorno != None):
                            print(retorno)   
                        else:
                            print("""
                            Nao foi encontrado 
                            nenhum registro com
                            esse identificador
                            """)        


                    elif opMenu == "2":
                        retorno = Menu.menuBuscaPorAtributo(d)
                        if(retorno != None):
                            print(retorno)   
                        else:
                            print("""
                            Nao foi encontrado 
                            nenhum registro com
                            esse identificador
                            """)        


                    elif opMenu == "0":
                        print("Voltando")

                opMenu = ""
               	print("""Não foi encontrado nenhum
               		registro com esse identificador""")

            elif opMenu == "2":

                print("entrou em Inserir")
                Menu.menuInserir(d)

            elif opMenu == "3":

                print("entrou em alterar")
                while opMenu != "0":
                    opMenu =  Menu.menuConsultar()
                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        if retorno != None:
                            Menu.menuAlterar(retorno,d)



                    elif opMenu == "2":
                        
                        retorno = Menu.menuBuscaPorAtributo(d)
                        if retorno != None:
                            Menu.menuAlterar(retorno,d) 


                    elif opMenu == "0":
                        print("Voltando")
                opMenu = ""

            elif opMenu == "4":

                print("entrou em deletar")
                while opMenu != "0":
                    opMenu =  Menu.menuConsultar()
                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        if retorno != None:
                            Menu.menuDeletar(retorno,d)

                    elif opMenu == "2":
                        retorno = Menu.menuBuscaPorAtributo(d)
                        if retorno != None:
                            Menu.menuDeletar(retorno,d)

                    elif opMenu == "0":
                        print("Voltando")
                opMenu = ""

            elif opMenu == "0":
                print("Saindo")

            elif opMenu == "":
                print("")

            else:
                print("Informe uma opcao valida")

    @staticmethod
    def menuBuscaPorIdentificador(d :Dados):
        retorno = d.buscarPorIdentificador(Validador.verificarInteiro())
        return retorno

    @staticmethod
    def menuBuscaPorAtributo(d :Dados):
        retorno = d.menuBuscaPorAtributo(input("Informe um tamanho para busca: "))
        return retorno

    def menuInserir(d):
    	# tipoDeRabo
    	#self._tamanho = tamanho
    #self._raca = raca
   # self._cor = cor
    #self._peso = peso

    	e = Ethios()
    	e.peso = input("Informe um peso: ")
    	e.tamanho = input("Informe o tamanho: ")
    	e.modelo = input("Informe o modelo: ")
    	e.temAerofolio = input("Informe se o carro tem aerofólio: ")
    	e.cor = input("Informe a cor: ")
    	d.inserir(e)

    def menuDeletar(retorno,d):

    	print(retorno)
    	deletar = input("Dejesa deletar ?")
    	if deletar == "S" or deletar == "s":
    		d.deletar(retorno)
    		print("""Registro deletado""")
    	else:
    		print("""
    			Registro não foi deletado
    			""")
       	# if deletar == "S" or deletar == "s":
       	# 	d.deletar(retorno)
      		# print("""
      		# Registro deletado
      		# """)
        # else:
        # 	print("""
        # 	Registro não foi deletado
        # 	""")
    def menuAlterar(retorno,d):

    	retorno.peso = Validador.validarValorInformado(retorno.peso,"Informe um peso: ")
    	retorno.tamanho = Validador.validarValorInformado(retorno.tamanho,"Informe o tamanho: ")
    	retorno.modelo = Validador.validarValorInformado(retorno.modelo,"Informe o modelo: ")
    	retorno.temAerofolio = Validador.validarValorInformado(retorno.temAerofolio,"Informe se o carro tem aerofólio: ")
    	retorno.cor = Validador.validarValorInformado(retorno.cor,"Informe a cor: ")
    	d.alterar(retorno)

