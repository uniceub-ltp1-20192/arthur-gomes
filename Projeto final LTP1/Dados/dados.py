from Entidades.entidadeFilha import Ethios
from Validador.validador import Validador

class Dados:

    def __init__(self):
        self._dados = dict()
        self.identificador = 0
        
    def buscarPorIdentificador(self,paramIdentificador):
        ''' len() retorna tamanho do dicionario/array'''
        if (len(self._dados) == 0):
            print("Dicionario vazio!")
        else:
            return self._dados.get(paramIdentificador)

    def buscarPorAtributo(self,param):
        lista = dict()        
        ultimoEncontrado = 0
        if (len(self._dados) == 0):
            print("Dicionario vazio!")
        else:
            for x in self._dados.values():
                if x.tamanho == param:
                    lista[x.identificador] = x
                    ultimoEncontrado = x.identificador
            if (len(lista) == 1):
                return lista[ultimoEncontrado]
            else:
                if (len(lista) == 0):
                    print("Nenhum encontrado")
                    return None
                return self.buscarPorIdentificadorComLista(lista)
    
    def buscarPorIdentificadorComLista(self,lista):
        print("Existem multiplos registros com o filtro informado:")
        for x in lista.values():
            print(x)
        print("Informe um identificador:")    
        return lista.get(int(Validador.verificarInteiro()))

            
    def inserir(self,entidade):
        '''Gerando o proximo identificador e colocando
        no atributo identificador da entidade'''
        print(vars(entidade))
        entidade.identificador = self.gerarProximoIdentificador()
        '''Salva a entidade dentro de uma posicao do
        dicionario utilizando o identificador'''
        self._dados[entidade.identificador] = entidade

    def alterar(self, entidade):
    	self._dados[entidade.identificador] = entidade                  
    
    def deletar(self,entidade):
        del self._dados[entidade.identificador]


    def gerarProximoIdentificador(self):
        '''incrementa +1 ao valor de identificador'''
        self.identificador = self.identificador + 1
        print(self.identificador)
        return self.identificador

    def buscarPorAtributo(self,param):
    	lista = dict()
    	ultimoEncontrado = 0
    	if (len(self._dados) == 0):
    		print("Dicionário vazio!")

    def buscarPorIdentificadorComLista(self,lista):
    	print("Existem múltiplos registros com o filtro informado:")
    	for x in lista.values():
    		print(x)
