pessoas = ["Isis", "Ananda", "Marcos"]

for i in range(len(pessoas)):
	print(pessoas[i] + ", você está convidada(o) para jantar comigo na próxima Segunda-Feira")

qtd_convidados = len(pessoas)
print("Quantidade de convidados:", qtd_convidados)	

print("Convidado que não pode comparecer:", "Isis")

pessoas = ["Cristina", "Ananda", "Marcos"]

qtd_convidados = len(pessoas)
print("Quantidade de convidados:", qtd_convidados)	

for i in range(len(pessoas)):
	print(pessoas[i] + ", você está convidada(o) para jantar comigo na próxima Segunda-Feira")
	
print("Encontrei uma mesa de jantar maior.")
pessoas.insert(0, "Pedro Akil")
pessoas.insert(2, "Fábio")
pessoas.append("Apri")

qtd_convidados = len(pessoas)
print("Quantidade de convidados:", qtd_convidados)	

for i in range(len(pessoas)):
	print(pessoas[i] + ", você está convidada(o) para jantar comigo na próxima Segunda-Feira")


print("Vou poder convidar apenas duas pessoas para o jantar.")

pessoa1 = pessoas.pop()
print(pessoa1 + ", não posso mais convidá-lo para jantar.")

pessoa2 = pessoas.pop(1)
print(pessoa2 + ", não posso mais convidá-lo para jantar.")

pessoa3 = pessoas.pop()
print(pessoa3 + ", não posso mais convidá-lo para jantar.")

pessoa4 = pessoas.pop()
print(pessoa4 + ", não posso mais convidá-lo para jantar.")

qtd_convidados = len(pessoas)
print("Quantidade de convidados:", qtd_convidados)	

for i in range(len(pessoas)):
	print(pessoas[i] + ", você está convidada(o) para jantar comigo na próxima Segunda-Feira")

del pessoas[1]
del pessoas[0]

print(pessoas)

qtd_convidados = len(pessoas)
print("Quantidade de convidados:", qtd_convidados)	