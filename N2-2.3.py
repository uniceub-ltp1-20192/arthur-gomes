cebolas = 500 #quantidade de cebolas 
cebolas_na_caixa = 130 #quantidade de cebolas dentro da caixa
espaco_caixa = 60 #espaço vazio da caixa
caixas = 200 #quantidade de caixas
cebolas_fora_da_caixa = cebolas - cebolas_na_caixa #a quantidade de cebolas fora da caixa é uma variável e atribuímos a ela o resultado da quantidade de cebolas menos a quantidade de cebolas na caixa
caixas_vazias = int(caixas - (cebolas_na_caixa/espaco_caixa)) #atribuímos à variável com a quantidade de caixas vazias o resultado da quantidade de caixas menos a divisão entre a quantidade de cebolas na caixa e o espaço vazio da caixa
caixas_necessarias = int(cebolas_fora_da_caixa / espaco_caixa) #a quantidade de caixas necessárias seria a quantidade de cebolas fora da caixa dividida pelo espaço vazio de cada caixa

print ("Existem " + str(cebolas_na_caixa) + " cebolas encaixotadas") #imprime na tela 'Existem 120 cebolas encaixotadas'
print ("Existem " + str(cebolas_fora_da_caixa) + " cebolas sem caixa") #imprime na tela 'Existem 180 cebolas sem caixa'
print ("Em cada caixa cabem " + str(espaco_caixa) + " cebolas") #imprime na tela 'Em cada caixa cabem 5 cebolas'
print ("Ainda temos, " + str(caixas_vazias) + " caixas vazias") #imprime na tela 'Ainda temos, 36 caixas vazias'
print ("Então, precisamos de " + str(caixas_necessarias) + " caixas para empacotar todas as cebolas") #imprime na tela 'Então, precisamos de 36 caixas para empacotar todas as cebolas'