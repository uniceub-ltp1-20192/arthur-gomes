from collections import Counter

def is_anagram(str1, str2):
    return Counter(str1.lower()) == Counter(str2.lower())

lista = ["ABC", "MNO", "DJK", "DEF"]
palavra = input("Informe uma palavra: ")

tamanho = 0

for char in palavra:
    tamanho += 1

for i in range(0, len(lista)):
    if is_anagram(palavra, lista[i]):
        print("O " + str(i+1) + "º elemento é um anagrama da palavra informada")
